
    let Assembly =
        Assembly [
            Namespace "WebSharper.ECharts" (Array.toList NamespaceEntities)
            Namespace "WebSharper.ECharts.Resources" [
                (Resource "echarts.js" "echarts.min.js").AssemblyWide()
            ]
        ]

[<Sealed>]
type Extension() =
    interface IExtension with
        member ext.Assembly =
            Definition.Assembly

[<assembly: Extension(typeof<Extension>)>]
do ()
