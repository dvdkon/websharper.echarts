#!/usr/bin/env sh

if [ $# -ne 1 ]; then
	echo "Usage: $0 <dts2ws folder>" 1>&2
	exit 1
fi

TEMP="$(mktemp)"

DTS2WS="$1"
pushd "$DTS2WS"
tsc
popd
node "$DTS2WS"/out/dts2ws.js \
	$(find typescript/ -name "*.d.ts") \
	--stripNamespace=echarts.EChartOption.BasicComponents \
	--stripNamespace=echarts.EChartOption \
	--stripNamespace=echarts \
	| sed -e 's/^\(.\)/    \1/g' \
	> "$TEMP"

cat _header.fs "$TEMP" _footer.fs > Main.fs
